import express, { NextFunction, Response, Request } from 'express';
const app = express()
import todoRoutes from "./routes/todo"

app.use(express.json())
app.use("/todos", todoRoutes)
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).json({ message: err.message })
})


app.listen(3000, () => {
    console.log("Successfully connected")
})